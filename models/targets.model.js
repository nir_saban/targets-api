const { configSql } = require("../config/index");
const DB = require("../classes/DB");
const { departments } = require("../data/departments.json");
const JsonBuilder = require("../classes/JsonBuilder");
const { jsonTypes } = require("../config/index");

var Targets = {};

Targets.getDashboard = async () => {
  let query = "EXEC [dbo].[Marketing_Target_Screen]";
  let db = new DB(configSql);
  const dashboardData = await db.sqlQueryPromise(query);
  console.log(dashboardData);
  return new JsonBuilder(jsonTypes.DASHBOARD, dashboardData).buildJson();
};

Targets.getDepartmentsCampaigns = async () => {
  const promises = [];
  let db = new DB(configSql);
  departments.map((department) => {
    let sqlQuery = `EXEC [dbo].[Marketing_Target_Screen] @Department = ${department}`;
    promises.push(db.sqlQueryPromise(sqlQuery));
  });
  let data = await Promise.all(promises);
  console.log(data);
  return new JsonBuilder(jsonTypes.CAMPAIGNS, data).buildJson();
};

Targets.getNewDeposit = async (id) => {
  let db = new DB(configSql);
  let sqlQuery = `exec [dbo].[Marketing_Target_Screen_by_AccountID] @transactionId=[${id}]`;
  let [depositData] = await db.sqlQueryPromise(sqlQuery);
 console.log(depositData);
  if (depositData) {
    return new JsonBuilder(jsonTypes.DEPOSIT, depositData).buildJson();
  } else {
    throw new Error("the deposit id not found");
  }
};

module.exports = Targets
