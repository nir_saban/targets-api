exports.deposit_campaignId = "CampaignID";
exports.deposit_department = "Department";
exports.deposit_name = "Owner";
exports.deposit_country = "CampaignLanguage";
exports.deposit_FTDAmount = {
  current: "$FTD",
};
exports.deposit_FTDCurrency = {
  current: "#FTD",
};
exports.deposit_leads = {
  current: "Leads",
};
exports.deposit_cr = {
  current: "CR",
};
exports.deposit_rr = {
  current: "RR",
};
exports.deposit_cpa = {
  current: "CPA",
};
