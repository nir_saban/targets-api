exports.dashboard_name = "Department";

exports.dashboard_FTDTotal = {
  part: "#FTD",
  target: "#FTDsTarget",
};
exports.dashboard_FTDAmount = {
  part: "$FTD",
  target: "$FTDsTarget",
};
exports.dashboard_leads = {
  part: "Leads",
  target: "LeadsTarget",
};
exports.dashboard_cr = {
  part: "CR",
  target: "CRTarget",
};
exports.dashboard_rr = {
  part: "RR",
  target: "RRTarget",
};
exports.dashboard_cpa = {
  part: "CPA",
  target: "CPATarget",
};

// const group = {
//   name: "Department",
//   rank: "Rank",
// };
// exports.mainStructure = {
//   departments: [],
//   rankFTDS: group,
//   group: true,
// };
