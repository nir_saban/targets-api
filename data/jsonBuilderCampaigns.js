const { departments } = require("./departments.json");
exports.campaigns_name = "Owner";

exports.campaigns_countries = "Languages";

exports.campaigns_FTDAmount = {
  target: "#FTDsTarget",
  current: "#FTD",
};
exports.campaigns_FTDCurrency = {
  current: "$FTD",
  target: "$FTDsTarget",
};
exports.campaigns_leads = {
  current: "Leads",
  target: "LeadsTarget",
};
exports.campaigns_cr = {
  current: "CR",
  target: "CRTarget",
};
exports.campaigns_rr = {
  current: "RR",
  target: "RRTarget",
};
exports.campaigns_cpa = {
  current: "CPA",
  target: "CPATraget",
};