const moment = require("moment");
var nodemailer = require("nodemailer");
const fs = require("fs");
const path = require("path");

const getDateTime = (format) => {
  return moment().format(format);
};
const LogErrors = (file, data) => {
  let contentLog = `----  ${data}  ---- time : ${getDateTime("Y-M-D H:m:s")} `;
  fs.appendFileSync(
    path.join(__dirname, "../logs") + "/" + file,
    contentLog + "\r\n"
  );
  // sendEmail(contentLog, file);
};
const sendEmail = (contentLog, subject) => {
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "nirsa11@gmail.com",
      pass: "parnasa2021",
    },
  });

  var mailOptions = {
    from: "nirsa11@gmail.com",
    to: "nirsa11@gmail.com",
    subject: subject.replace(".txt", ""),
    text: contentLog,
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};
module.exports = {
  getDateTime,
  LogErrors,
  sendEmail,
};
