const chai = require("chai");
const chaiHttp = require("chai-http");
const { getTargetsData } = require("../controllers/targets.controller");
const Targets = require("../models/targets.model");
const RabbitMq = require("../classes/RabbitMq");
const {configRabbit} = require("../config/index");
const {rabbitMsg}  = require("./requestsTest.json");


chai.should();

chai.use(chaiHttp);

describe("get targets data", () => {
  it("it should get object like {dashboard: {},departments:{}}", async () => {
    let response = await getTargetsData();
    response.should.be.a("object");
    Object.keys(response).length.should.be.eq(2);
    Object.keys(response["departments"]).length.should.be.eq(3);
  });
});


describe("new Deposit", () => {
  it("it should get new deposit array", async () => {
    let response = await Targets.getNewDeposit(
      "3380A8AC-966C-EC11-80D5-0050560AC1BF"
    );
    response.should.be.a("array");
    Object.keys(response[0]).length.should.be.eq(10);
  });
});


describe("Test Rabbit consumer", () => {
  it("it should get new rabbitmq message",  () => {      
    consumeRabbit(publishMessage,(data) => {
      response.should.be.a("object");
    })
});
});


const consumeRabbit = (publishMessage,callbackMsg) => {  
  new RabbitMq(configRabbit).consumeExchangeMsg(
   process.env.RABBIT_EXCH,
   process.env.RABBIT_QUEUE,
   (data) => {
     callbackMsg(data)
   }
  )
  publishMessage()
}


const publishMessage = () => {
  new RabbitMq(configRabbit).publishMessage(
    process.env.RABBIT_EXCH,
    process.env.RABBIT_QUEUE,
    JSON.stringify(rabbitMsg)
  );
}