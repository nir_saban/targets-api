const amqp = require("amqplib/callback_api");
const {LogErrors} = require("../helpers/index");

class RabbitMq {
  constructor(config) {
    this.config = config;
  }

  publishMessage(exchange, queue, msg) {
    amqp.connect(this.config, function (error0, connection) {
      if (error0) {
        throw error0;
      }
      connection.createChannel(function (error1, channel) {
        if (error1) {
          throw error1;
        }
        channel.assertExchange(exchange, "fanout", {
          durable: true,
        });
        channel.sendToQueue(queue, Buffer.from(msg), {
          exclusive: false,
        });
        console.log(" [x] Sent %s:'%s'", queue, msg);
      });

      setTimeout(function () {
        connection.close();
        process.exit(0);
      }, 500);
    });
  }
  consumeExchangeMsg(exchange, queue, callBack) {
    amqp.connect(this.config, function (error0, connection) {
      if (error0) throw error0;
      connection.createChannel(function (error1, channel) {
        if (error1) throw error1;
        channel.assertExchange(exchange, "fanout", {
          durable: true,
        });
        channel.assertQueue(
          queue,
          {
            exclusive: false,
          },
          function (error2, q) {
            if (error2) {
              throw error2;
            }
            console.log(
              " [*] Waiting for messages in %s. To exit press CTRL+C",
              q.queue
            );
            channel.bindQueue(q.queue, exchange, "");
            channel.consume(
              q.queue,
              function (msg) {
                if (msg.content) {
                  console.log('received new message');
                  let obj = JSON.parse(msg.content.toString())
                  LogErrors("rabbitLog.txt", obj.value.transactionId );
                  callBack(obj);
                }
              },
              {
                noAck: true,
              }
            );
          }
        );
      });
    });
  }
}

module.exports = RabbitMq;
