const { departments } = require('../data/departments.json')
const dashboardSchema = require('../data/jsonBuilderDashboard')
const campaignsSchema = require('../data/jsonBuilderCampaigns')
const depositSchema = require('../data/jsonBuilderDeposit')

class JsonBuilder {
  constructor(type, data) {

    this.type = type
    this.data = data
    this.debbug = type == 'campaigns'
    this.objSchemas = {
      dashboardSchema,
      campaignsSchema,
      depositSchema
    }
  }
  buildJson() {
    switch (this.type) {
      case 'dashboard':
        return this.orderDashboardData()
      case 'campaigns':
        return this.orderCampaignsData()
      case 'deposit':
        return this.structureBuilder([this.data])
      default:
        throw new Error('json type not found')
    }
  }
  
  orderDashboardData() {
    const data = {
      departments: this.structureBuilder(this.data),
      ranksFTD: this.data
        .map((item) => ({
          name: item.Department,
          ftd: parseInt(item['#FTD'])
        }))
        .filter(({ name }) => name != 'Total')
    }
    return data
  }

  orderCampaignsData() {
    let newData = {}
    departments.map((department, index) => {
      newData[department.toLowerCase()] = {
        name: department,
        campaigns: this.structureBuilder(this.data[index])
      }
    })
    return newData
  }

  // getting an  array and return the correct structure
  structureBuilder(data) {

    let objSchema = this.type + 'Schema'
    let newData = data.map(row => {
      let newRow = {}
      for (let i in this.objSchemas[objSchema]) {
        let field = i.replace(this.type + '_', '')
        if (typeof this.objSchemas[objSchema][i] != 'object') {
          newRow[field] = field == 'countries' ? row[this.objSchemas[objSchema][i]].split(',') :  row[this.objSchemas[objSchema][i]]
        } else {
          newRow[field] = {}
          for (const [key, value] of Object.entries(
            this.objSchemas[objSchema][i]
          )) {
            newRow[field][key] = row[value]
          }
        }
      }
      
      return newRow
    })
    return newData
  }

}

module.exports = JsonBuilder
