var nodeSQL = require('node-sql');


class DB {

    constructor(config) {
        this.config = config
    }

    sqlQueryPromise(query) {
        return new Promise((resolve, reject) => {
            nodeSQL.exec(query, this.config, function (err, result) {
                if (err) reject(err)
                resolve(result)
            });
        })
    }
}


module.exports = DB