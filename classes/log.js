const fs = require("fs");
const path = require("path")
const {getDateTime} = require("../helpers/index")
const LogErrors = (file , data) => {
    fs.appendFileSync(
         path.join(__dirname,"../logs") + "/" + file,
            '----' + data +  ` ---- time : ${getDateTime("Y-M-D H:m:s")} `  + "\r\n"
       );
}




module.exports = LogErrors