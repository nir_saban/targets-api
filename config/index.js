exports.configSql = {
  userName: process.env.userNameDB,
  password: process.env.password,
  server: process.env.server,
  port: process.env.DBport,
  options: {
    database: process.env.database,
  },
};

exports.jsonTypes = {
  DASHBOARD: "dashboard",
  CAMPAIGNS: "campaigns",
  DEPOSIT: "deposit",
};

exports.configRabbit = {
  hostname: process.env.RABBIT_URL,
  username: process.env.RABBIT_USER,
  password: process.env.RABBIT_PASSWORD,
  vhost: process.env.RABBIT_VHOST,
};
