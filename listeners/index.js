const { getTargetsData } = require("../controllers/targets.controller");

module.exports = (io) => {
  io.on("connection", async (client) => {
    console.log("neww Connection !!");
    io.emit("message", await getTargetsData());
  });
  setInterval( async() => {
    console.log("interval runing %s" , new Date().toJSON().substring(0,19).replace('T',' ')) ;
    io.emit("message", await getTargetsData());
  },3600000)
};
