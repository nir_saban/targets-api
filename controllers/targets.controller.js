const RabbitMq = require("../classes/RabbitMq");
const { configRabbit } = require("../config");
const fs = require("fs");

const Targets = require("../models/targets.model");
const {LogErrors} = require("../helpers/index");

const getTargetsData = async () => {
  try {
    const dashboard = await Targets.getDashboard();
    const departments = await Targets.getDepartmentsCampaigns();
    return {
      dashboard,
      departments,
    };
  } catch (error) {
    console.log({ error });
    return error.message;
  }
};

const consumeRabbitMsg = (io) => {
  new RabbitMq(configRabbit).consumeExchangeMsg(
    process.env.RABBIT_EXCH,
    process.env.RABBIT_QUEUE,
    (data) => rabbitMsgCallback(data, io)
  );
};

const rabbitMsgCallback =  (objMessage, io) => {
  setTimeout(async() => {
    try {
      let [depositData] = await Targets.getNewDeposit(objMessage.value.transactionId);
      if (Object.keys(depositData).length > 0 && !objMessage.test) {
        io.emit("new_deposit", depositData);
        io.emit("message", await getTargetsData());
      } else {
        LogErrors("emptyDeposit.txt", objMessage.value.transactionId);
      }
    } catch (error) {
      LogErrors("errorDeposit.txt", error.message +  " " + objMessage.value.transactionId);
    }
  },10000)
};

module.exports = {
  getTargetsData,
  consumeRabbitMsg,
};