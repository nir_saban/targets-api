const express = require('express')
require('custom-env').env('production')
const cors = require('cors')
const app = express()
const socketio = require('socket.io')
const initListeners = require('./listeners')
const { consumeRabbitMsg} = require('./controllers/targets.controller')
const PORT = process.env.PORT || 5000
const server = app.listen(PORT, () => {
  console.log(`app listen to ${PORT}`)
})

const io = socketio(server, {
  cors: {
    origin: '*'
  }
})

app.use(cors())
app.use(express.json())

//start socket  connection
initListeners(io)

//start listen to rabbit queue 
consumeRabbitMsg(io);